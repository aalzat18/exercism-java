class ReverseString {

    String reverse(String inputString) {
        String answer = new StringBuilder(inputString).reverse().toString();
        return answer;
    }
  
}