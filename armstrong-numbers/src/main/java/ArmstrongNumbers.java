class ArmstrongNumbers {

	boolean isArmstrongNumber(int numberToCheck) {

		return numberToCheck == calculateArmstrongNumber(numberToCheck);
	}

	int calculateArmstrongNumber(int numberToCheck){
		int result = 0;
		int digits = numberOfDigits(numberToCheck);

		while (numberToCheck > 9) {
			result += (int) Math.pow(firstDigit(numberToCheck), digits);
			numberToCheck = numberToCheck/10;
		}

		result += (int) Math.pow(firstDigit(numberToCheck), digits);
		
		return result;
	}

	int firstDigit(int numberToCheck){
		while (numberToCheck > 9) {
			numberToCheck = numberToCheck/10;
		}
		return numberToCheck;
	}

	int numberOfDigits(int numberToCheck){
		int number = 1;
		while (numberToCheck > 9) {
			numberToCheck = numberToCheck/10;
			number += 1;
		}
		return number;
	}
}
