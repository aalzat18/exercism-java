class RnaTranscription {

    String transcribe(String dnaStrand) {
        int aux = 0;
        StringBuilder answer = new StringBuilder(dnaStrand);
        for (char var : dnaStrand.toCharArray()) {
            switch (var) {
                case 'G':
                    answer.setCharAt(aux, 'C');
                    break;

                case 'C':
                    answer.setCharAt(aux, 'G'); 
                    break;

                case 'T':
                    answer.setCharAt(aux, 'A');
                    break;

                case 'A':
                    answer.setCharAt(aux, 'U');
                    break;

                default:
                    break;
            }
            aux += 1;
        }
        return answer.toString();
    }

}
