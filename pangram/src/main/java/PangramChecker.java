import java.util.ArrayList;

public class PangramChecker {

    public boolean isPangram(String input) {
        ArrayList<Character> alphabet = new ArrayList<Character>();
        for (char var : input.toCharArray()) {
            int cont = 0;
            if (Character.isLetter(var)) {
                for (char in : alphabet) {
                    if (var == in) {
                        cont += 1;
                    }
                }
                if(cont == 0){
                    alphabet.add(var);
                }
            }
        }

        return alphabet.size()==26;
    }

}
